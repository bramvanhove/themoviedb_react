
import './App.css';
import React, { Component } from 'react';

import { Header } from "./components/Header";
import { Footer } from "./components/Footer";


import { Home } from "./pages/Home";
import { Movie } from "./pages/Movie";
import { Login } from "./pages/Login";
import { Profile } from "./pages/Profile";

import FirebaseConf from './config/FirebaseConf';
import { BrowserRouter as Router, Route } from "react-router-dom";


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      user: null,
    });
    this.authListener = this.authListener.bind(this);
  }

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    FirebaseConf.auth().onAuthStateChanged((user) => {
      console.log("user", user);
      if (user) {
        this.setState({ user });
        localStorage.setItem('user', user.uid);
        localStorage.setItem('userEmail', user.email);
        
      } else {
        this.setState({ user: null });
        localStorage.removeItem('user');
        localStorage.removeItem('userEmail');
      }
    });
  }

  render(){
    return(
      /*<div>{this.state.user ?   <Home/> : (<Login />)}</div>*/
      <Router>
        <div>
          <Header />
          <div className="App">
            <Route exact path="/" component={Home}  />
            <Route path="/movielist/:sortOption" component={Home}  params="sortOption"/>
            <Route path="/movie/:id" component={Movie} params="id" />
            <Route path="/movie" component={Movie} />
            <Route path="/login" component={Login} />
            
            <Route path="/profile" component={Profile} params={this.state.user}/>

          </div>
          <Footer />
        </div>
      </Router>
    )
  }
}

/*
const NotFound = () => (
    <div>
      <h1>Sorry, can’t find Page.</h1>
    </div>
)*/