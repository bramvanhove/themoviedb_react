import firebase from "firebase";

var config = {
    apiKey: "AIzaSyCOjJzLwE8YTawYaIlmmLuE3RuysmlA96w",
    authDomain: "moviereactapp-7d2d4.firebaseapp.com",
    databaseURL: "https://moviereactapp-7d2d4.firebaseio.com",
    projectId: "moviereactapp-7d2d4",
    storageBucket: "moviereactapp-7d2d4.appspot.com",
    messagingSenderId: "181549668984"
};

const firebaseConf = firebase.initializeApp(config);

export default firebaseConf;