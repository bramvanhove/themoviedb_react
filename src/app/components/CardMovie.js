import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import './components.css';
import Popup from "reactjs-popup";
import { MoviesSeparatService } from "./MoviesSeparatService";
import { IoMdCloseCircleOutline, IoIosHeartEmpty, IoIosHeart } from 'react-icons/io';
import firebase from 'firebase';

export default class CardMovie extends Component {
    //props:
    //index, poster_path, title, id, lengte=[]

  constructor(props){
    
    super(props);
    //var date = new date();

    console.log(this.props.border);
  }

  heart(id){
    if (localStorage.user) {
      firebase.database().ref(localStorage.user + '/favoriteMovies/' + id).set({
        "MovieId": id,
        "date": Date("dd/mm/yyyy")
      })
    }
  }
  
  disHeart(id){
    console.log('not love', localStorage.user + '/favoriteMovies/' +id);
    firebase.database().ref(localStorage.user + '/favoriteMovies/').child(id).remove()
  }

  render() {
    
      var card = (
        <Col xs={this.props.lengte[0]} className="card"  md={this.props.lengte[1]} lg={this.props.lengte[2]} 
        style={{marginBottom: "20px"}} >

            <div className={this.props.border ? 'parent right love' :'parent right' } key={this.props.index}>
                <div className="child" style={{backgroundImage: "url(https://image.tmdb.org/t/p/w500"+ this.props.poster_path + ")"}}>
                    <div>
                      {this.props.title}
                    </div>
                    
                    <p style={{marginBottom: "-20px", color: "#fff", backgroundColor: "rgb(34, 34, 34, 0.5)", textAlign: "left", paddingLeft: "10px", paddingRight: "10px"}}>
                      { this.props.index + 1 }
                    </p>
                </div>
            </div>

        </Col>
      )
    return (
      <Popup
        trigger={card}
        modal
        closeOnDocumentClick
        lockScroll={true}
      >{close => (
        
        <div style={{overflowY: "scroll", overflowX: "hidden", height: "90vh", width: "100%"}} > 
        
          <a className="close" onClick={close}>
           <IoMdCloseCircleOutline size={40} color="#000" />
          </a>

          {
            this.props.border ? 
              <a className="close heart" onClick={this.disHeart.bind(this, this.props.id)} >
                <IoIosHeart size={40} color="#000" />
              </a> 
              :

              localStorage.user ?
                <a className="close heart" onClick={this.heart.bind(this, this.props.id)} >
                  <IoIosHeartEmpty size={40} color="#000" />
                </a>
                :
                <a className="close heart" href="/login">
                  <IoIosHeartEmpty size={40} color="#000" />
                </a>
          }

          <MoviesSeparatService id={this.props.id}/> 
        </div>
      )}
      </Popup>
    );
  }
}