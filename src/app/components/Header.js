import React, { Component } from 'react';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import FirebaseConf from '../config/FirebaseConf';


export class Header extends Component {

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }


    logout() {
      FirebaseConf.auth().signOut();
      localStorage.removeItem('user');
    }

    render() {
        return (
            <Navbar inverse collapseOnSelect style={{marginBottom: "-20px"}}>
                <Navbar.Header>

                    <Navbar.Brand>
                        <a href="/movielist/popularity.desc">Bram Test React</a>
                    </Navbar.Brand>
                    
                    <Navbar.Toggle />

                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={1} href="/Movie" >
                            Random Movie
                        </NavItem>

                        <NavItem eventKey={2} href="#">
                            Link
                        </NavItem>

                        <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1}>Action</MenuItem>
                            <MenuItem eventKey={3.2}>Another action</MenuItem>
                            <MenuItem eventKey={3.3}>Something else here</MenuItem>
                            <MenuItem divider />
                            <MenuItem eventKey={3.3}>Separated link</MenuItem>
                        </NavDropdown>

                    </Nav>

                    <Nav pullRight>
                    {localStorage.user ?   
                        <NavItem eventKey={1} href="/profile">
                            Profile
                        </NavItem> :
                        <NavItem eventKey={2} href="/login">
                            Register
                        </NavItem>
                    }
                    {localStorage.user ?   
                        <NavItem eventKey={1} href="/" onClick={this.logout}>
                                Logout
                        </NavItem> : 
                        <NavItem eventKey={2} href="/login">
                            login
                        </NavItem>
                    }
                    </Nav>


                </Navbar.Collapse>
            </Navbar>
        );
    }
}

