import React, { Component } from 'react';
import YouTube from 'react-youtube';

export class YoutubePlayer extends Component {

    render() {
        var youtube, movieYoutube = this.props.movieYoutube;
        
        if (movieYoutube !== undefined){                
            youtube = (
                <YouTube
                    videoId= {movieYoutube.key}
                    opts=
                    {{
                        height: '390',
                        width: '100%',
                        playerVars: {autoplay: 1}
                    }}
                />
            )
        }else{
            youtube = <p style={{color: "#fff"}}>no video</p>
        }

        return (
            youtube
        );
    }
}

