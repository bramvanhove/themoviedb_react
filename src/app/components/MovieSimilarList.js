import React, { Component } from 'react';
import  CardMovie  from "./CardMovie";

export class MovieSimilarList extends Component {

    render() {

        var moviesSimilar = this.props.moviesSimilar, movieSimilarListHeader, movieSimilarList

        
        
        if (moviesSimilar) {

            movieSimilarList =(
                
                moviesSimilar.map((item, i) => {
                    
                    if (i < 18) {
                    return( 
                        <CardMovie index={i} poster_path={item.poster_path} title={item.title} id={item.id} lengte={[6,4,3]} key={i} page={1}/>
                    )}
                })
            ); 
            console.log(moviesSimilar[0]);
            
            if (moviesSimilar[0]) {
                movieSimilarListHeader = (
                    <div>
                        <div
                            style={{
                                color: "rgb(34, 34, 34)",
                                backgroundColor: "#fff",
                                height: 2,
                                margin: "10px -20px 10px -20px"
                            }}
                        />
                        <h2 style={{color: "#fff"}}>Similar movies:</h2> 
                    </div>
                );
            }
        }

        return (
            <div>
                {movieSimilarListHeader}
                {movieSimilarList}
            </div>
        );
    }
}

