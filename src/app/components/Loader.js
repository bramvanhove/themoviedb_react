import React, { Component } from 'react';
import RingLoader from 'react-spinners/RingLoader';

export default class Loader extends Component {
  render() {
    return (
        <RingLoader
            css={`
            display: block;
            margin: 200px auto;
            `}
            sizeUnit={"px"}
            size={150}
            color={this.props.color}
        />
    );
  }
}