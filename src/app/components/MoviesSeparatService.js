import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Container } from 'reactstrap';
import  DetailsMovie  from "./DetailsMovie";

import Loader from './Loader';
import './components.css';

import { YoutubePlayer } from "./YoutubePlayer";
import { MovieSimilarList } from "./MovieSimilarList";

import MovieDbConf from "../config/MovieDbConf";

export class MoviesSeparatService extends Component {    
    constructor(props) {
        super(props);
    
        this.state = {
            movies: [],
            movieYoutube: [],
            moviesSimilar: [],

            louding: true,
            reqwest: true,
        };
    }
    
    componentDidMount() {
        this.UserList();
    }

    componentDidUpdate(){
        if (this.state.reqwest) {
            this.UserList();
        }
    }

    UserList() {
        console.log("props", this.props);
        
        var url = MovieDbConf.dbUrl + 'movie/'+ this.props.id
        +'?api_key=' + MovieDbConf.key 
        +'&language=' + MovieDbConf.language 
      
        var urlSimilar = MovieDbConf.dbUrl + 'movie/'+ this.props.id
        +'/similar?api_key=' + MovieDbConf.key 
        +'&language=' + MovieDbConf.language 
        +'&page=1'

        var urlYoutube = MovieDbConf.dbUrl + 'movie/'+ this.props.id
        +'/videos?api_key=' + MovieDbConf.key 
        +'&language=' + MovieDbConf.language 
        
        console.log(["url", url],["urlSimilar", urlSimilar], ["urlYoutube", urlYoutube]);
        
        fetch(urlSimilar)
            .then(response => response.json())
            .then(data => {
                this.setState({ moviesSimilar: data.results })

                fetch(urlYoutube)
                    .then(response => response.json())
                    .then((data) =>{ 
                        
                        if (data.results) {
                            this.setState({movieYoutube: data.results[0]}) 
                        }                       

                        fetch(url)
                            .then(response => response.json())
                            .then((data) => this.setState({ movie: data , louding: false}));
                    });
            });

        this.setState({reqwest: false})
    }
    
    render() {
        const { movie, moviesSimilar, movieYoutube } = this.state; 

        var movieDetails, genreList;

        if (!this.state.louding && movie.genres) {
            //////////// genreList:
            genreList = (
                movie.genres.map((genre, i) =>
                (
                    <Col md={4} key={i}>
                        <b>{genre.name}</b>
                    </Col>
                ))
            )           

            //////////// movieDetails:
            movieDetails = <DetailsMovie movie={movie} genreList={genreList}/>
        }else{
            movieDetails = <div><Loader color={"#ffffff"}/></div>
        }
        
        return (
            <Container style={{backgroundColor: "rgb(34, 34, 34)", width: "100%"}}>

                <Row >
                    <Col>
                        {movieDetails}
                    </Col>
                </Row>

                <Row >
                    <Col>
                        <YoutubePlayer  movieYoutube={movieYoutube}/>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <MovieSimilarList moviesSimilar={moviesSimilar}/>
                    </Col>
                </Row>

            </Container>
        );
    }
}