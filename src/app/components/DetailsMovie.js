import React, { Component } from 'react';
import './components.css';
import { Col, Row } from 'react-bootstrap';
import { CardImg, CardBody } from 'reactstrap';

export default class DetailsMovie extends Component {
    

  render() {

    var movie = this.props.movie
    var genreList = this.props.genreList

    return (
        <div>
            <Row >
                <div style={{ backgroundImage:"url(https://image.tmdb.org/t/p/w1280"+ movie.backdrop_path + ")", backgroundPosition: 'center',
                backgroundSize: 'cover',backgroundRepeat: 'no-repeat',  height: "350px", marginLeft:"15px", marginRight:"15px"}} >
                    {/* kop foto */}
                </div>

                <div style={{marginTop: "-100px"}}>
                    <Col md={5}>
                        <div style={{height: "390px", width: "100%"}}>
                            <CardImg top style={{border: '10px solid rgb(34, 34, 34)'}}  height="390px"  src={"https://image.tmdb.org/t/p/w500"+ movie.poster_path} alt={movie.title} />
                        </div>
                    </Col>

                    
                    <Col md={6} style={{backgroundColor: "#fff", border: '10px solid rgb(34, 34, 34)' }}>
                        <CardBody>
                            <div>
                                <h3 style={{padding: "10px", backgroundColor: "rgb(34, 34, 34)", color: "#fff", margin: "-1px -15px 10px -15px"}}>{ movie.title }</h3>
                            </div>
                        </CardBody>
                    
                        <CardBody>
                            <Row >
                                <Col md={6}>
                                    <b>Runtime: {movie.runtime} min <br/>
                                    Release date: {movie.release_date}</b>
                                </Col>

                                <Col md={6}>
                                    {genreList}
                                </Col>
                                <Col md={12}>
                                
                                    <div
                                        style={{
                                            color: "rgb(34, 34, 34)",
                                            backgroundColor: "rgb(34, 34, 34)",
                                            height: 2,
                                            margin: "10px -20px 10px -20px"
                                        }}
                                    />

                                    <h4>{ movie.tagline }</h4>
                                    <p>{ movie.overview }</p>
                                </Col>
                            </Row>
                        </CardBody>

                    </Col>
                </div>
            </Row>
        </div>
    );
  }
}