import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import Loader from '../components/Loader';

export class ListLoader extends Component {
    render() {
        return (
            <Col xs={4} className="card"  md={4} lg={2} style={{marginBottom: "20px"}} >

                <div className="parent" key={this.props.index}>
                    <div style={{marginTop: "-100px", marginBottom: "-200px"}}><Loader color={"rgb(34, 34, 34)"}/></div>
                    <div className="fixedDiv">{this.props.message}</div>
                </div>

            </Col>
        );
    }
}

