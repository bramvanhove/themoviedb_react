import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Button, Container } from 'reactstrap';
import Loader from '../components/Loader';
import  CardMovie  from "../components/CardMovie";
import '../components/components.css';
import firebase from 'firebase';
import MovieDbConf from "../config/MovieDbConf";
import { ListLoader } from "../components/ListLoader";

export class Home extends React.Component {    
    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    
        this.state = {
            sortOptions: [{name: "Most Popular", url_key: "popularity.desc"},{name: "Least Popular", url_key: "popularity.asc"}, {name: "Newest", url_key: "release_date.desc"}, {name: "Eldest", url_key: "release_date.asc"}],
            zoeken: "",
            movies: [],

            sort_by: "popularity.desc",
            page: 1,

            louding: true,
            update: false,
            message:'... Schroll down ...',

            favoriteMovies: []

        };
        var _this = this
        
        if (localStorage.user) {
            firebase.database().ref(localStorage.user + '/favoriteMovies/').on('value', function (snapshot) {
                if (snapshot.val()) {
                    _this.setState({favoriteMovies: Object.keys(snapshot.val())})
                }
            });
        }
        
    }

    handleScroll() {
        if (!this.state.louding) {
            const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            const body = document.body, html = document.documentElement;
            const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
            const windowBottom = windowHeight + window.pageYOffset + 500;
            if (windowBottom >= docHeight) {
            
                this.setState({
                    message:'loading more...', page: this.state.page + 1, update: true
                });
                console.log(this.state.page);
                
            } else {
                this.setState({
                    message:'... Schroll down ...'
                });
            }
        }
    }
    
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
        if (this.props.match.params.sortOption)  this.setState({sort_by: this.props.match.params.sortOption, update: true})
        else this.setState({sort_by: "popularity.desc", update: true})
    }

    componentWillUnmount() {
      window.removeEventListener("scroll", this.handleScroll);
    }

    componentDidUpdate(){
        if (this.state.update === true)  this.fetchList()
    }

    fetchList() {

        var url;
        if (this.state.zoeken !== "") {
            url = MovieDbConf.dbUrl + 'search/movie?'
            +'api_key=' + MovieDbConf.key 
            +'&query=' + this.state.zoeken
            +'&language=' + MovieDbConf.language 
            +'&include_adult=false&include_video=false&page=' + this.state.page;
        }else{
            url = MovieDbConf.dbUrl + 'discover/movie?'
            +'api_key=' + MovieDbConf.key 
            +'&language=' + MovieDbConf.language 
            +'&sort_by=' + this.state.sort_by 
            +'&include_adult=false&include_video=false&page=' + this.state.page;
            
        }
        
        console.log("url", url);
        
        fetch(url)
            .then(response => response.json())
            .then(data =>{
                console.log(data);
                this.setState({ movies: [...this.state.movies, ...data.results.filter(result=> result.poster_path !== null )], louding: false, update: false })
                return
            });

        if (this.state.page < 3)  this.setState({page: this.state.page + 1})
    }

    render() {
        const { movies } = this.state;  
        var moviesList, listLouder;

        if (!this.state.louding) {
            moviesList = movies.map((item, i) => (
                <CardMovie border={this.state.favoriteMovies.some( id => id == item.id )} key={i} index={i} poster_path={item.poster_path} title={item.title} id={item.id} lengte={[4,4,2]} page={this.state.page} />
            ));

        }else{
            moviesList= <div><Loader color={"rgb(34, 34, 34)"}/></div>
        }
        
        return (
            <div>
                <Container onScroll={this.handleScroll} className="fixedDiv">

                    <Row >
                        <Col style={{marginBottom: "10px", marginTop: "30px"}}>
                            <h3>
                                Filter By: {this.sortbutton()}
                                <input  type="text" value={this.state.zoeken} onChange={this.handleChange.bind(this)}></input>
                            </h3>
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col style={{minHeight: "900px"}}>
                            {moviesList}
                            <ListLoader message={this.state.message}/>
                        </Col>
                    </Row>
                    
                </Container>
            </div>
        );
    }

    handleChange(event) {
        this.setState({zoeken: event.target.value, update: true, page: 1, movies: []});
    }

    sortbutton(){
        const buttons = this.state.sortOptions.map((item, i) => (
            <Button href={"/movielist/" + item.url_key} key={i}  style={{marginRight: "5px"}}> {item.name} </Button>
        ));

        return buttons;
    }

    newSort(sort_url_key){
        this.setState({sort_by: sort_url_key, page: 1, louding: true, zoeken: ""});
    }
}