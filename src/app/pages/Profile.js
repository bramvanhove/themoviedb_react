import React, { Component } from 'react';
import FirebaseConf from '../config/FirebaseConf';
import firebase from "firebase";
import  CardMovie  from "../components/CardMovie";
import MovieDbConf from "../config/MovieDbConf";
import { Col, Row } from 'react-bootstrap';
import { Button, Container } from 'reactstrap';
import { IoMdCloseCircleOutline, IoIosHeartEmpty, IoIosHeart } from 'react-icons/io';
import Loader from '../components/Loader';

export class Profile extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        
        this.state = {
            user: firebase.auth().currentUser,
            favoriteMovies: [],
            movies: [],
            loadin: true
        }

        var _this = this
        var keyss = [], movies = []
        if (localStorage.user) {
            firebase.database().ref(localStorage.user + '/favoriteMovies/').on('value', function (snapshot) {
                if (snapshot.val()) {
                    keyss = Object.keys(snapshot.val())
                    console.log("keyss", keyss);
                    movies = [];
                    keyss.map((id, i ) => {
                        var url = MovieDbConf.dbUrl + 'movie/'+ id
                        +'?api_key=' + MovieDbConf.key 
                        +'&language=' + MovieDbConf.language; 

                        console.log(url);
                        
                        fetch(url)
                        .then(response => response.json())
                        .then(data => {
                            movies =  [...movies, data]
                        }).then(e => {     
                            console.log(movies.length, keyss.length);
                                                   
                            if ( movies.length == keyss.length) {
                                _this.setState({favoriteMovies: keyss, loadin: false, movies: movies})
                            }
                        })
                    })

                }
            });

        }
    }
   
    componentWillMount(){
        console.log("movies",this.state.movies);
        
        if (!localStorage.user) {
            this.props.history.push('/login');
        }
    }

    logout() {
        FirebaseConf.auth().signOut();
    }

    render() {
        var fav = "";
        if (!this.state.loadin) {
            
            console.log("card", this.state.movies);
            
                console.log("11111");
                
                fav = this.state.movies.map((item, i) => (
                    <CardMovie border={this.state.favoriteMovies.some( id => id == item.id )} key={i} index={i} poster_path={item.poster_path} title={item.title} id={item.id} lengte={[4,4,2]} page={this.state.page} />
                ));
            
            
        }else{
            fav =  <div><Loader color={"rgb(34, 34, 34)"}/></div>
        }
        
        console.log("fav", fav);
        console.log("movies", this.state.movies);
        

        return (
            <div>
                <Container onScroll={this.handleScroll} className="fixedDiv">

                    <Row >
                        <Col style={{marginBottom: "10px", marginTop: "30px"}}>
                            <h1>{ localStorage.userEmail}</h1>
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col>
                            <h1>you favorite movies <IoIosHeart size={40} color="#000" style={{marginBottom: "-10px"}} />: </h1>
                            {fav}
                        </Col>
                    </Row>
                    
                </Container>
            </div>
        );

    }

}