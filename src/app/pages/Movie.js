import React, { Component } from 'react';
import { MoviesSeparatService } from "../components/MoviesSeparatService";

export class Movie extends Component {

  

  render() {

    var id

    if (!this.props.match.params.id) {
      id = parseInt( 100000 + (Math.random() * (500000-100000)));
    }else{
      id = this.props.match.params.id
    }

    return (
      <div className="App" style={{marginTop: "-20px"}}>

        <MoviesSeparatService id={id}/>
        
      </div>
    );
  }
}

